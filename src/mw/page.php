<?php namespace SpecialInvite\mw;
/*
  NOTES:
    During development, tried descending Page from SpecialCreateAccount and LoginSignupSpecialPage before settling on SpecialPage.
  HISTORY:
    2018-03-21
      * minor updates for latest Ferreteria
      * now displays proper text on Special:SpecialPages
    2018-07-01 Changing $wgOptCP_Subst* globals to constants, because the globals weren't working
    2021-07-18 I think the above history is referring to a different SpecialPage that I used as a model...
      This was working back in September of last year, but now there are bugs and you can't create an invite code.
      Fixed some bugs, working on others.
    2024-09-06 moved from invite-page.php to src/app/mw/page.php
*/

use SpecialPage as mwSpecialPage;
##
use SpecialInvite\cOptions as Options;
use SpecialInvite\data\row\cInfo as CardInfoClass;
#use SpecialInvite\data\row\iInfo as RowIface;
use SpecialInvite\data\row\cStatus as CardStatusClass;
use SpecialInvite\data\row\iStatus as CardStatusIface;
use SpecialInvite\data\rows\cInfo as DeckInfoClass;

class cPage extends mwSpecialPage {

    // ++ SETUP ++ //

    /*----
      OVERRIDE
      PURPOSE: Defines the URL-name of the page as shown in Special:SpecialPages
    */
    // OVERRIDE SpecialCreateAccount
    public function __construct() { mwSpecialPage::__construct('InviteCodes'); }
    /*----
      OVERRIDE
      PURPOSE: Defines which i18n message to display on Special:SpecialPages
    */
    function getDescription() { return $this->msg('invite-desc'); }
   
    // -- SETUP -- //
    // ++ CALCULATION ++ //

    /*----
      THINKING:
        1. If we're hiding the invite ( GetDoHideInvite() ), then we don't put the invite code
          in the URL here; it gets stuffed into the Session data elsewhere.
        2. This function is *slightly* overloaded in that it's used both to display the URL to admins
          *and* for the actual redirect, in non-hide mode, when a valid code is entered. The latter
          needs to know the code (sometimes), but the former doesn't -- so we'll just assume that
          if no code is passed, it's not needed  because we only want the URL for informational purposes.
    */
    protected function GetURL_forCreateAccountPage_withInvite(string $svInvite) : string {
        $snInvite = Options::GetName_forInviteString();
        $mwoTitleCreate = Options::GetTitle_forCreateAccount();
        if (Options::GetDoHideInvite() || is_null($svInvite)) {
            $urlQuery = '';
        } else {
            $sTitleSelf = 'Special:InviteCodes';  // TODO: get title-string from $this
            #$urlQuery = "$snInvite=$svInvite&returnto=$sTitleSelf"; // This returns to SpecialInvite after account creation (do not want).
            $urlQuery = "$snInvite=$svInvite";
        }
        $url = $mwoTitleCreate->getLocalUrl($urlQuery);
        return $url;
    }
    // USAGE: for informational purposes only
    protected function GetURL_forCreateAccountPage_forInfo() : string {
        $mwoTitleCreate = Options::GetTitle_forCreateAccount();
        $url = $mwoTitleCreate->getLocalUrl($urlQuery);
        return $url;
    }
    /**
     * NOTE:
     *  2024-09-06 This was formerly only used if cOptions::GetDoHideInvite() is TRUE.
     *    It has apparently not been used/tested
     */
    protected function SaveInviteToSession(string $svInvite) {
        $snInvite = Options::GetName_forInviteString();
        $mwoSess = $this->GetRequest()->getSession();   // MediaWiki\Session\Session
        $mwoSess->set($snInvite,$svInvite);   // save the invite code so we can check it again when create-account asks
        #throw new \exception('TO BE FINISHED');
    }
    
    // -- CALCULATION -- //
    // ++ EVENTS ++ //

    /*----
      PURPOSE: entrypoint into the extension
    */
    function execute( $sSubPage ) {
        $os = $this->InviteStatus();
        $ok = $os->GetIsValid();
        if ($ok) {
            // we have a valid invite, so show the account-request form

            $svInvite = urlencode($os->GetCode());
            if (Options::GetDoHideInvite()) {
                $this->SaveInviteToSession($svInvite);
                // TODO 2024-09-06: I think if we're using this option, then we want to remove the code from $svInvite too.
            }
            $urlCreate = $this->GetURL_forCreateAccountPage_withInvite($svInvite);
            #echo "urlCreate=[$urlCreate]"; die();
            $this->getOutput()->redirect($urlCreate);   // redirect to create-account page, but give it the invite code
        } else {
            $out = '';
            $mwoOut = $this->getOutput();
        
            $mwoOut->setPageTitle('Invite Code Handler');

            // check to see if user can administer invite codes
            
            $mwoUser = $this->getUser();
            $canMakeUsers = $mwoUser->isAllowedToCreateAccount();
            if ($canMakeUsers) {
                $mwoOut->addSubtitle("add, edit, test, and remove invite codes");
                // user can manage invite codes - display that UI
                $out .= "<h2>Manage Invite Codes</h2>\n"
                  .$this->DoAdminCodes()
                  .'<hr>'
                  ;
            } else {
                $mwoOut->addSubtitle("enter an invite code to create an account");
            }
            // user cannot manage invite codes - only allow them to submit a code for verification
            $out .= "<h2>Enter an Invite Code</h2>"
              .$this->DoInviteCheckPage()
              ;
            $mwoOut->AddHTML($out);	// display invite code stuff
        }
    }
    
    // -- EVENTS -- //
    // ++ INPUT ++ //
    
    private $osInvite = NULL;
    protected function InviteStatus()  : CardStatusIface {
        if (is_null($this->osInvite)) {
            $mwoReq = $this->getRequest();
            if ($mwoReq->getBool('btnCheck')) {
                $snInvite = Options::GetName_forInviteString();
                $svInvite = strtoupper($mwoReq->getText($snInvite));
                $os = DeckInfoClass::Me()->CheckCodeStatus($svInvite);
                echo 'SAVING INVITE STATUS, CODE='.$os->GetCode().'<br>';
            } else {
                $os = new CardStatusClass;
                $os->SetIsKnown(FALSE);
            }
            $this->osInvite = $os;
        }
        return $this->osInvite;
    }
    
    // ++ INPUT ++ //
    // ++ OUTPUT ++ //

    // TODO: rename as something like DoInviteStatusPage()
    protected function DoInviteCheckPage() : string {
        $os = $this->InviteStatus();
        $out = '';
        if ($os->GotACode()) {
            if ($os->GetIsValid()) {
                // This function shouldn't be executed if a valid invite code is received
                throw new \exception('Internal error: this code should never be executed.');
            } else {
                $sCode = $os->GetCode();
                $sErr = $os->GetError();
                // code not valid -- display error
                $out .= "Sorry! The invite code '$sCode' $sErr.<br>";
            }
        }
        $snInvite = Options::GetName_forInviteString();
        $out .= "<form method=POST>Enter an invite code: <input name=$snInvite size=20> <input type=submit name=btnCheck value='Request Account'></form>\n";
        return $out;
    }
    protected function DoAdminCodes() : string {
        $out = '';
        $oi = DeckInfoClass::Me();
        if ($oi->GetIsLoaded()) {
            // check for form input
            if ($this->getRequest()->getBool('btnSave')) {
                $this->SaveAdminForm(); // NOTE: execution will redirect at this point
            } else {
                $arCodes = $oi->GetCodeArray();
                $out .= $this->RenderAdminForm($arCodes);
            }
        } else {
            $os = $oi->GetStatus();
            $out .= $os->GetMessage();
        }
        $sSummary = $oi->GetSummaryMessage();
        $urlRedirect = $this->GetURL_forCreateAccountPage_forInfo();
        $sRedirect = Options::GetNameString_forCreateAccount();
        $out .= "<hr><i><small>$sSummary &sect; a valid code will redirect to <a href='$urlRedirect'>$sRedirect</a></small></i>";
        
        return $out;
    }
    protected function RenderAdminForm(array $arCodes) : string {
        $mwoReq = $this->getRequest();
        $oRow = new CardInfoClass;
    
        $out = "<form method=POST><table class='wikitable'>\n"
          . $oRow->ShowTableHeader()
          ;
        
        $idReq = $mwoReq->GetText('id');
        $idEdit = NULL;
        //$out .= "idEdit=[$idEdit]<br>";
        if ($idReq != '') {
            // take some action on a specific existing record
            $sDo = $mwoReq->getText('do');
            switch ($sDo) {
              case 'del':
                $rs = DeckInfoClass::Me();
                $this->DeleteRow($idReq);
                $rs->SaveCodesFile(); // TODO 2021-07-20 is this call redundant?
                $this->Redirect_toRemoveQuery();
                break;
              case 'edit':
                $idEdit = $idReq;
                break;
            }
        } else {
            // not acting on an existing record, so show form for entering a new one
            $out .= $oRow->EditTableRow();
        }
    
        // show existing records
        if (count($arCodes) > 0) {
            foreach ($arCodes as $id => $arCodeRecord) {
                $oRow->SetValueArray($arCodeRecord);
                if ($id == $idEdit) {
                    $out .= $oRow->EditTableRow();
                } else {
                    $out .= $oRow->ShowTableRow();
                }
            }
        } else {
            $out .= "<tr><td colspan=4>No invite codes have been saved yet.</td>\n";
        }
        $out .= "</table></form>\n";
        
        return $out;
    }
    protected function SaveAdminForm() {
        $mwoReq = $this->getRequest();
        $mwoUser = $this->getUser();
        $mwoOut = $this->getOutput();
        $rs = DeckInfoClass::Me();

        $rcNew = new CardInfoClass;
        $rcNew->ReadInput($mwoUser, $mwoReq);
        $sKey = $rcNew->GetCode();
        $rcOld = $rs->GetCodeRecord($sKey);
        
        #echo 'BEFORE - '.$this->DumpRows();
        $rs->LogChange($rcOld,$rcNew);
        $rs->SetCodeRecord($rcNew);
        #echo 'AFTER - '.$this->DumpRows(); die();
        
        $rs->SaveCodesFile(); // TODO 2021-07-20 is this call redundant?
        $this->Redirect_toRemoveQuery();
    }
    /*----
      ACTION: redirects to the same url, but removes any ?query (and of course POST data)
      PURPOSE: This means that any subsequent manual page-reloads will not re-submit the same data/action.
    */
    protected function Redirect_toRemoveQuery() {
        // self-redirect to clear form input:
        $oURL = cURL::FromRequest();
        $urlClean = $oURL->GetValue_Path();
        $this->getOutput()->redirect($urlClean);
    }
    
    // -- OUTPUT -- //

}
