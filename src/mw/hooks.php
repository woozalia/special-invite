<?php namespace SpecialInvite\mw;
/*
  HISTORY:
    2020-09-07 created
    2024-09-07 Fixing the new "can't actually create an account" bug --
      - solution seems to be whitelisting CreateAccount whenever there's a valid code
      - the onUserLoadAfterLoadFromSession( hook seems to be called at the right time for doing this, but
        - in order to check the invite code, I need to know what the user submitted...
        - ...which is in the Request...
        - ...which is only passed in through the onBeforeInitialize() hook...
        - ...which for some reason is hooked *after* onUserLoadAfterLoadFromSession()...
        - ...so it looks like I should update the whitelist inside onBeforeInitialize()
            (and not bother with onUserLoadAfterLoadFromSession()).
*/

use MediaWiki\Actions\ActionEntryPoint as mwEntryPoint;
use OutputPage as mwOutputPage;
use Title as mwTitle;
use User as mwUser;
use WebRequest as mwWebRequest;
##
use SpecialInvite\data\rows\cInfo as DeckInfoClass;
use SpecialInvite\cOptions as Options;

class cHooks {

    // ++ HOOKS ++ //

    /**
      TODO: need to set information about the Title in onBeforeInitialize()
      HISTORY:
        2021-07-21 Removed the conditional EnableSignup() and PersistInviteCode() calls
          with a note that "this seems to actually be unnecessary."
    */
    static public function onBeforeInitialize(
      mwTitle &$title,
      $unused,
      mwOutputPage $output,
      mwUser $user,
      mwWebRequest $request,
      mwEntryPoint $mediaWiki ){

        self::SetTitle($title);
        self::SetRequest($request);

        if (self::IsCreateAccountPage()) {  // rules only apply to CreateAccount
            if (self::HasValidInvite()) {
                self::EnableSignup($user);
                self::PersistInviteCode();
            }
        }

        #echo 'VALID INVITE?['.self::HasValidInvite().']<br>';
        #die('GOT TO '.__FILE__.' line '.__LINE__);

    }
    /*----
      PURPOSE: This is (maybe?) needed so that stuff gets set up properly when it's an API call.
      INPUT:
        &$main: ApiMain object
    */
    public static function onApiBeforeMain( &$main ) {
        self::$isCreateAcctPage = FALSE;
    }

    /*----
      ACTION: If URL requests the Create Account page *and* the user has a valid invite,
        then temporarily give the user access to the page.
    */
    static public function onUserGetRights(mwUser $user, array &$aRights ) {
        if (self::IsCreateAccountPage()) {
            if (self::HasValidInvite()) {
                $aRights[] = 'read';
                $aRights[] = 'createaccount';
                $aRights[] = 'deletedhistory'; // I don't know why it needs this permission, but lack of it causes an error
            }
        }
    }
    
    /**
     * PURPOSE: "Called immediately after a local user has been created and saved to the database"
     * DOCS: https://www.mediawiki.org/wiki/Manual:Hooks/LocalUserCreated
     */
    public static function onLocalUserCreated( $user, $autocreated ) {
        if (self::HasValidInvite()) {
            self::EnableSignup($user);
            self::ForgetInviteCode();
        }
    }
    
    // -- HOOKS -- //
    // ++ CONDITIONS ++ //
    
    static private $isCreateAcctPage = NULL;  // NULL = needs to be determined
    static protected function IsCreateAccountPage() : bool {
        if (is_null(self::$isCreateAcctPage)) {
        
            if (self::HasTitle()) {
                $mwoTitle = self::GetTitle();
            
                $sLog = "TITLE: ".$mwoTitle;
                self::$isCreateAcctPage = $mwoTitle->equals( Options::GetTitle_forCreateAccount() );
            } else {
                $sLog = "No title; assume API loader.";
                self::$isCreateAcctPage = FALSE;
            }
            
        }
        return self::$isCreateAcctPage;
    }
    
    static private $hasValidInvite = NULL;
    static protected function HasValidInvite() {
        if (is_null(self::$hasValidInvite)) {
            self::$hasValidInvite = FALSE;    // assume invalid until proved valid
            $snInvite = Options::GetName_forInviteString();
            $svInvite = NULL;
            $mwoReq = self::GetRequest();
            if (Options::GetDoHideInvite()) {
                $mwoSess = $mwoReq->getSession();

                if ($mwoSess->exists($snInvite)) {
                    $svInvite = $mwoSess->get($snInvite);
                }
            } else {
                if ($mwoReq->getBool($snInvite)) {
                    $svInvite = $mwoReq->getText($snInvite);
                } else {
                    $svInvite = $mwoReq->getCookie($snInvite,NULL);
                }
            }
            if (is_null($svInvite)) {
                #throw new \exception('Special:Invite internal error: invite code was not successfully passed here.');
                // TODO: maybe log this as a hacking attempt?
            } else {
                $oi = DeckInfoClass::Me();
                $os = $oi->CheckCodeStatus($svInvite);
                if ($os->GetIsValid()) {
                    self::$hasValidInvite = TRUE;     // invite is valid
                }
            }
        }
        return self::$hasValidInvite;
    }
    
    // -- CONDITIONS -- //
    // ++ OBJECTS ++ //
    
    static private $mwoTitle = NULL;
    static protected function SetTitle(mwTitle $mwoTitle) { self::$mwoTitle = $mwoTitle; }
    static protected function HasTitle() : bool { return !is_null(self::$mwoTitle); }
    static protected function GetTitle() : mwTitle { return self::$mwoTitle; }
    
    static private $mwoReq;
    static protected function SetRequest(mwWebRequest $mwoReq) { self::$mwoReq = $mwoReq; }
    static protected function GetRequest() : mwWebRequest { return self::$mwoReq; }
    
    // -- OBJECTS -- //
    // ++ ACTIONS ++ //
    
    /**
     * ACTION: give the current user (session) access to the signup process
     * NOTES:
     *  * 2020-09-14 This may be redundant.
     */
    static protected function EnableSignup(mwUser $user) {
        // Make sure user can access the CreateAccount page in a private wiki
        global $wgWhitelistRead;
        if ( !is_array( $wgWhitelistRead ) ) {
            $wgWhitelistRead = [];
        }
        array_push( $wgWhitelistRead, Options::GetNameString_forCreateAccount() );
        // or, alternatively (untested):
        #$wgWhitelistRead[] = Options::GetTitle_forCreateAccount();
    }
    /*----
      ACTION: make sure the invite code will still be accessible after the create-user request
        is received, so we can once again authorize the user
      ASSUMES:
        HIDE = FALSE: the invite code has been passed via Request (currently GET)
        HIDE = TRUE: the invite code is already in the Session; no action needed
      NOTE: There is some overlap between this and ::InviteStatus()
    */
    static protected function PersistInviteCode() {
        if (Options::GetDoHideInvite()) {
            // invite code is already in the Session; don't need to get it, leave it alone
        } else {
            $mwoReq = self::GetRequest();
            $mwoResp = $mwoReq->response();
            $snInvite = Options::GetName_forInviteString();
            $svInvite = $mwoReq->getText($snInvite);
            $mwoResp->setCookie($snInvite,$svInvite);
        }
    }
    static protected function ForgetInviteCode() {
        if (Options::GetDoHideInvite()) {
            throw new \exception('To be written');
        } else {
            $snInvite = Options::GetName_forInviteString();
            $mwoReq = self::GetRequest();
            $mwoResp = $mwoReq->response();
            $mwoResp->clearCookie($snInvite);
        }
    }
}
