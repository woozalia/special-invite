<?php namespace SpecialInvite\data\row;
/*
  PURPOSE: handles data & UI for individual codes
  HISTORY:
    2020-09-02 [invite-row.php] created
    2020-09-07 [invite-row.php] moved cInviteStatus here from invite.page
    2024-09-06 Split invite-row.php into data/row/status.php and data/row/info.php
      Renamed [SI]cCodeRecord ->[SI]data\row\cInfo
      Added iInfo
*/

use User as mwUser;
use WebRequest as mwWebRequest;
##
use SpecialInvite\data\row\cStatus as StatusClass;
use SpecialInvite\data\row\iStatus as StatusIface;

interface iInfo {
    // DATA
    function SetValueArray(array $ar);
    function GetValueArray() : array;

    function GetCode() : string;
    function SetCode(string $s);

    function GetCreator() : string;
    function SetCreator(string $s);

    function GetValidAfterString() : string;
    function SetValidAfterString(string $s);

    function GetValidBeforeString() : string;
    function SetValidBeforeString(string $s);

    // CALCULATION
    function GetValidityStatus() : StatusIface;
    // INPUT
    function ReadInput(mwUser $mwoUser, mwWebRequest $mwoReq);
    // OUTPUT
    static function ShowTableHeader();
    function ShowTableRow() : string;
    function EditTableRow() : string;

}
class cInfo implements iInfo {

    // ++ SETUP ++ //

    // field names
    protected const KS_FIELD_CODE = 'code';
    protected const KS_FIELD_CREATOR = 'created-by';
    protected const KS_FIELD_WHEN_START = 'when-start';
    protected const KS_FIELD_WHEN_STOP = 'when-stop';

    // -- SETUP -- //
    // ++ FORMATS ++ //

    static protected function DateStringToIntOrBlank(string $s) {
        if ($s == '') {
            return '';
        } else {
            return strtotime($s);
        }
    }
    static protected function DateIntOrNullToString(?int $d) : string {
        #$n = (int)$d;
        #echo "DATE VALUE=[$n]";
        if (is_int($d)) {
            return date('Y-m-d H:i',$d);
        } else {
            return '';
        }
    }

    // -- FORMATS -- //
    // ++ DATA ++ //

    private $arVals = array();
    public function SetValueArray(array $ar) { $this->arVals = $ar; }
    public function GetValueArray() : array { return $this->arVals; }

    protected function GetInt(string $sName) : ?int {
        if (array_key_exists($sName,$this->arVals)) {
            $v = (int)$this->arVals[$sName];
            return $v;
        } else {
            return NULL;
        }
    }

    protected function GetString(string $sName) : string {
        if (array_key_exists($sName,$this->arVals)) {
            return $this->arVals[$sName];
        } else {
            return '';
        }
    }
    protected function SetString(string $sName, string $sValue) { $this->arVals[$sName] = $sValue; }

    public function GetCode() : string { return $this->GetString(self::KS_FIELD_CODE); }
    public function SetCode(string $s) { $this->SetString(self::KS_FIELD_CODE,$s); }

    public function GetCreator() : string { return $this->GetString(self::KS_FIELD_CREATOR); }
    public function SetCreator(string $s) { $this->SetString(self::KS_FIELD_CREATOR,$s); }

    protected function GetValidAfter() { return $this->arVals[self::KS_FIELD_WHEN_START]; }
    public function GetValidAfterString() : string {
        $sRaw = $this->GetInt(self::KS_FIELD_WHEN_START);
        $sConv = self::DateIntOrNullToString($sRaw);
        return $sConv;
    }
    public function SetValidAfterString(string $s) {
        $this->SetString(
          self::KS_FIELD_WHEN_START,
          self::DateStringToIntOrBlank($s)
          );
    }

    protected function GetValidBefore() { return $this->arVals[self::KS_FIELD_WHEN_STOP]; }
    public function GetValidBeforeString() : string {
        return self::DateIntOrNullToString(
          $this->GetInt(
            self::KS_FIELD_WHEN_STOP
            )
          );
    }
    public function SetValidBeforeString(string $s) {
        $this->SetString(
          self::KS_FIELD_WHEN_STOP,
          self::DateStringToIntOrBlank($s)
          );
    }

    // -- DATA -- //
    // ++ CALCULATION ++ //

    public function GetValidityStatus() : StatusIface {
        $os = new StatusClass;
        $os->SetCode($this->GetCode()); // make sure the status includes the invite code
        $os->SetIsValid(TRUE);
        $nNow = time();

        $vValidAfter = $this->GetValidAfter();
        if (!empty($vValidAfter)) {
            if ($nNow < $vValidAfter) {
                $os->SetIsValid(FALSE);
                $os->SetError('is not yet valid.');
            }
        }

        $vValidBefore = $this->GetValidBefore();
        if (!empty($vValidBefore)) {
            if ($nNow > $vValidBefore) {
                $os->SetIsValid(FALSE);
                $os->SetError('has expired.');
            }
        }
        return $os;
    }

    // -- CALCULATION -- //
    // ++ INPUT ++ //

    public function ReadInput(mwUser $mwoUser, mwWebRequest $mwoReq) {
        $this->SetCode(       strtoupper($mwoReq->getText(self::KS_FIELD_CODE)) );
        $this->SetCreator(    $mwoUser->getName()                               );

        $sTime = trim($mwoReq->getText(self::KS_FIELD_WHEN_START));
        #echo "STIME=[$sTime] CONVERTED=[".self::DateStringToIntOrBlank($sTime)."]<br>";die();
        $this->SetValidAfterString($sTime);

        $sTime = trim($mwoReq->getText(self::KS_FIELD_WHEN_STOP));
        $this->SetValidBeforeString($sTime);
    }

    // -- INPUT -- //
    // ++ OUTPUT ++ //

    static public function ShowTableHeader() {
        $out = <<<__END__
<tr>
  <th>Invite Code</th>
  <th>created by</th>
  <th>only valid after</th>
  <th>only valid until</th>
</tr>
__END__;

        return $out;
    }
    public function ShowTableRow() : string {
        $sCode = $this->GetCode();
        $sUser = $this->GetCreator();
        $sOkAfter = $this->GetValidAfterString();
        $sOkBefore = $this->GetValidBeforeString();

        $out = <<<__END__
<tr>
  <td>$sCode</td>
  <td>$sUser</td>
  <td>$sOkAfter</td>
  <td>$sOkBefore</td>
  <td><b>[</b><a href="?do=edit&id=$sCode">edit</a><b>]</b></td>
</tr>
__END__;

        return $out;
    }
    public function EditTableRow() : string {
        // field names
        $snCode = self::KS_FIELD_CODE;
        $snCreator = self::KS_FIELD_CREATOR;
        $snWhenStart = self::KS_FIELD_WHEN_START;
        $snWhenStop = self::KS_FIELD_WHEN_STOP;

        // field values
        $svCode = htmlspecialchars($this->GetCode());
        $svUser = htmlspecialchars($this->GetCreator());
        $svOkAfter = htmlspecialchars($this->GetValidAfterString());
        $svOkBefore = htmlspecialchars($this->GetValidBeforeString());

        if ($svCode != '') {
            $oURL = cURL::FromRequest();
            $urlClean = $oURL->GetValue_Path();
            $htCancel = " <b>[</b><a href='$urlClean'>CANCEL</a><b>]</b>";
            $htDel = " <b>[</b><a href='?do=del&id=$svCode'>DELETE</a><b>]</b>";
        } else {
            $htCancel = '';
            $htDel = '';
        }

        $out = <<<__END__
<tr>
  <td><input name=$snCode size=10 value="$svCode" required></td>
  <td align=center><i>you</i></td>
  <td><input name=$snWhenStart size=10 value="$svOkAfter"></td>
  <td><input name=$snWhenStop size=10 value="$svOkBefore"></td>
  <td><input type=submit name=btnSave value="Save">$htDel$htCancel</td>
</tr>
__END__;

        return $out;
    }

    // -- OUTPUT -- //
}
