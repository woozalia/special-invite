<?php namespace SpecialInvite\data\row;
/*
  HISTORY:
    2020-09-02 [invite-row.php] created
    2020-09-07 [invite-row.php] moved cInviteStatus here from invite.page
    2024-09-06 Split invite-row.php into data/row/status.php and data/row/info.php
      renamed [SI]cInviteStatus -> [SI]data\row\cStatus
      added iStatus
*/

interface iStatus {
    function SetIsValid(bool $b);
    function GetIsValid() : bool;
    function SetIsKnown(bool $b);
    function GetIsKnown() : bool;
    function SetError(string $s);
    function GetError() : string;
    function SetCode(string $s);
    function GetCode() : string;
    function GotACode() : bool;
}

class cStatus implements iStatus {
    private $isValid = FALSE; // the token can be used?
    public function SetIsValid(bool $b) { $this->isValid = $b; }
    public function GetIsValid() : bool { return $this->isValid; }
    
    private $isKnown = FALSE; // the matches an existing record (whether valid or not)?
    public function SetIsKnown(bool $b) {
        $this->isKnown = $b;
        if (!$b) {
            $this->SetIsValid(FALSE);
        }
    }
    public function GetIsKnown() : bool { return $this->isKnown; }
    
    private $sErr = NULL;
    public function SetError(string $s) { $this->sErr = $s; }
    public function GetError() : string { return $this->sErr; }
    
    private $sCode = NULL;
    public function SetCode(string $s) { $this->sCode = $s; }
    public function GetCode() : string { return $this->sCode; }
    public function GotACode() : bool { return !is_null($this->sCode); }
}

