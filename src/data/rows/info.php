<?php namespace SpecialInvite\data\rows;
/*
  HISTORY:
    2020-09-07 created - splitting off data classes from invite.page: moved cInvites here
    2024-08-11 removing Ferreteria dependencies (or attempting to, anyway) - starting with Singleton
    2024-09-06 moved from [SI]cInvites (invite.rows.php) to [SI]data\rows\cInfo (info.php)
*/

use SpecialInvite\cOptions as Options;
use SpecialInvite\data\row\cInfo as RowInfoClass;
use SpecialInvite\data\row\iInfo as RowinfoIface;
use SpecialInvite\data\row\cStatus as CardStatusClass;
use SpecialInvite\data\row\iStatus as CardStatusIface;
use SpecialInvite\util\cAction as ActionClass;
use SpecialInvite\util\iAction as ActionIface;

class cInfo {

    // ++ SETUP ++ //

    static public $oMe = NULL;
    static public function Me() : self { return self::$oMe ?? (self::$oMe = new self); }

    public function __construct() {
        self::$oMe = $this;
        $this->LoadData();
    }
    
    private $os;
    private $sSummary;
    protected function LoadData() {
        $fs = Options::GetSpec_forCodes();
        $bExists = file_exists($fs);
        
        if ($bExists) {
            $os = $this->LoadCodesFile($fs);
            $ok = $os->GetOkay();
            if (!$ok) {
                $sIssue = $os->GetMessage();
            }
        } else {
            // file not created yet
        
            $os = new ActionClass;
            // confirm it can be created, so user doesn't waste time editing and then not being able to save
            $ok = @touch($fs);
            if (!$ok) {
                $os->SetMessage('The invite codes file could not be created! The web host needs some configuration.');
                $sIssue = 'it could not be created.';
            }
        }

        if ($ok) {
            $sVerb = $bExists?'are':'will be';
            $this->sSummary = "Invite Codes $sVerb stored in: <b>$fs</b>";
        } else {
            $this->sSummary = "There was a problem with the invite codes file <b>$fs</b>: $sIssue";
        }
        $this->os = $os;
    }
    public function GetStatus() : ActionIface { return $this->os; }
    public function GetSummaryMessage() : string { return $this->sSummary; }
    
    // -- SETUP -- //
    // ++ DATA ++ //

    private $arCodes = array();
    /*----
      ASSUMES: LoadCodesFile() has been called
      PUBLIC so the Page can get the data to display
    */
    public function GetCodeArray() : array {
        return $this->arCodes;
    }
    /*----
      HISTORY:
        2021-07-18 changed protected->public because this is called from outside
        2021-07-20 can now return NULL if existing record not found
    */
    public function GetCodeRecord(string $sKey) : ?RowInfoIface {
        $rc = NULL;
        if (array_key_exists($sKey,$this->arCodes)) {
            $ar = $this->arCodes[$sKey];
            if (is_array($ar)) {
                $rc = new RowInfoClass;
                $rc->SetValueArray($ar);
            }
        }
        return $rc;
    }
    /*----
      HISTORY:
        2021-07-20 changing protected->public because this is *only* called from outside
    */
    public function SetCodeRecord(RowInfoIface $rc) {
        $sKey = $rc->GetCode(); // all codes must be unique, so using that as a key
        $this->arCodes[$sKey] = $rc->GetValueArray();

        /*
        // DEBUGGING
        $mwoReq = $this->getRequest();
        echo 'REQ:'.\fcArray::Render($mwoReq->getPostValues());
        echo 'DATA:'.\fcArray::Render($this->arCodes); die();
        */
    }
    protected function InviteExists(string $sInvite) : bool { return array_key_exists($sInvite,$this->GetCodeArray()); }

    // -- DATA -- //
    // ++ STORAGE ++ //

    /*----
      HISTORY:
        2021-07-20 changed protected->public because this is *only* called from outside
    */
    public function SaveCodesFile() {
        $fs = Options::GetSpec_forCodes();
        $js = json_encode($this->arCodes);
        file_put_contents($fs,$js);
    }
    private $isLoaded = FALSE;
    protected function LoadCodesFile() : ActionIface {
        $fs = Options::GetSpec_forCodes();
        $os = new ActionClass;
    
        $js = file_get_contents($fs);
        if ($js === FALSE) {
            $os->SetOkay(FALSE);
            // this will probably generate an exception if it happens, actually, so this is more a place-marker:
            $os->SetMessage("There was an error reading the invite codes data file.");
        } else {
            $ar = json_decode($js,TRUE);  // decode as associative array
            if (is_array($ar)) {
                $os->SetOkay(TRUE);
                $this->arCodes = $ar;
                $this->isLoaded = TRUE;
            } else {
                $nLen = strlen($js);
                if ($nLen > 0) {
                    $os->SetOkay(FALSE);
                    $sLen = $nLen . ' bytes';
                    $os->SetMessage("The invite-codes data ($sLen) was read, but could not be decoded.");
                } else {
                    // file is empty -- no codes yet
                    $os->SetOkay(TRUE);
                    $this->arCodes = [];
                    $this->isLoaded = TRUE;
                }
            }
        }
        return $os;
    }
    public function GetIsLoaded() : bool { return $this->isLoaded; }

    // -- STORAGE -- //
    // ++ ACTION ++ //

    /*----
      ACTION: saved submitted row data to in-memory all-rows array
        Does not write to permanent storage.
    */
    protected function SaveRow(RowInfoIface $rc) {
        throw new \exception('Call SetCodeRecord() instead');
    }
    protected function DeleteRow(string $id) {
        unset($this->arCodes[$id]);
    }
    /*----
      NOTE:
        2021-07-20 not sure why I'm bothering to pass rcOld and rcNew, since nothing is done with them.
          Future plan to log old and new values, maybe?
    */
    public function LogChange(?RowInfoIface $rcOld, RowInfoIface $rcNew) {
    /* 2021-07-20 this won't actually work as written (e.g. $user is undefined); research needed.
        $logEntry = new \ManualLogEntry( 'Invites', 'user' ); // Log action 'bar' in the Special:Log for 'foo'
        $logEntry->setPerformer( $user ); // User object, the user who performed this action
        $logEntry->setTarget( $this ); // The page that this log entry affects, a Title object
        $logEntry->setComment( $reason ); // Optional, user provided comment
    */
    }
    
    // -- ACTION -- //
    // ++ CALCULATION ++ //
    
    // PUBLIC because Page needs to be in charge of fetching input from the form
    public function CheckCodeStatus(string $sInvite) : CardStatusIface {
        $os = new CardStatusClass;
        $os->SetCode($sInvite);
        if ($this->InviteExists($sInvite)) {
            $os->SetIsKnown(TRUE);
            $rc = $this->GetCodeRecord($sInvite);
            $os = $rc->GetValidityStatus();
        } else {
            $os->SetIsKnown(FALSE);
            $os->SetError('is not recognized.');
            #echo $this->DumpRows();
        }
        return $os;
    }
    
    // -- CALCULATION -- //
    // ++ DEBUGGING ++ //
    
    protected function DumpRows() : string {
        return 'INVITE CODE RECORDS:'.\fcArray::Render($this->GetCodeArray());
    }

    // -- DEBUGGING -- //
}
