<?php namespace SpecialInvite\util;
/*
  HISTORY:
    2014-04-02 [Ferreteria] Created clsURL from methods in clsHTTP
    2016-11-20 [Ferreteria] Renamed clsURL to fcURL.
    2017-02-04 [Ferreteria] Removed RemoveBasePath() (deprecated alias for PathRelativeTo()).
    2017-05-13 [Ferreteria] Changing internals: now stores parsed value instead of raw value.
      This makes it easier to operate on the pieces.
    2024-08-11 adapted from Ferreteria code [formerly] used by this extension
*/

interface iURL {
    static function FromRequest() : self;

    function SetValue(string $url);
    function GetValue()       : string;
    function GetValue_Path()  : string;
}

class cURL implements iURL {

  // // ++ DYNAMIC ++ // //

    // ++ SETUP ++ //

    public function __construct(string $url) {	$this->SetValue($url); }

    static public function FromRequest() : iURL { return new static(self::RequestString()); }

    // -- SETUP -- //
    // ++ VALUE ++ //

    private $url;
    public function SetValue(string $url) { $this->url = $url; }
    public function GetValue()      : string { return $this->url; }
    public function GetValue_Path() : string { return parse_url($this->GetValue(),PHP_URL_PATH); }

    // -- VALUE -- //

  // // -- DYNAMIC -- // //
  // // ++ STATIC ++ // //

    // ++ ENVIRONMENT ++ //

    static protected function RequestString() {
        return $_SERVER["REQUEST_URI"];
    }
    /*
    static
        $sClass = __CLASS__;	// LATER: Replace this with a method so descendant classes can spawn themselves
        return new $sClass(self::GetCurrentString());
    }
    */

    // -- ENVIRONMENT -- //
    // ++ CALCULATIONS ++ //

    /*----
      TODO: This should probably be renamed -- it doesn't *just* remove the base,
        it also removes the ?query and #fragment. Something like NonBasePathOnly()
        would be accurate but clumsy.
      USED BY PathInfo functions
      NOTES:
        parse_url() doesn't properly handle paths that begin with a segment
          that looks like a port number on a domain, e.g. something:34. I'm
          therefore not using it to strip off ?query and #fragment anymore.
          It can be made to work by tacking on a scheme and domain (http://dummy.com)
          at the beginning, but that seems more klugey than just truncating the string
          after the first "?" or "#".
      HISTORY:
        2014-04-02 Rewritten; renamed from RemoveBaseURI() to RemoveBasePath()
        2014-04-27 Found bug in something but forgot to finish making a note about it.
        2017-02-04 Rewrote substantially; parts didn't make sense. Fixing even though it ain't broke.
          * Using self::GetCurrentString()
          * Nobody is using $wpFull and it's not clear exactly how it would be used, so I'm removing it.
          * Now returning NULL if $urlBase is not part of current path.
      TODO: Do we need special handling for $urlBase='/', or does it work out ok?
    */
    /* 2024-08-11 Does not seem to be used by Invite.
    static public function PathRelativeTo($urlBase) {
        $wpFull = self::GetCurrentObject()->GetValue_Path();
        if ($urlBase == '') {
            // base is root = nothing to calculate
            $wpOut = $wpFull;
        } else {
            $wpBase = parse_url($urlBase,PHP_URL_PATH);		// BASE: just the path
            $wpFullRel = ltrim($wpFull,KS_CHAR_PATH_SEP);
            $wpBaseRel = ltrim($wpBase,KS_CHAR_PATH_SEP);

            if ($wpBaseRel == '') {
            $idx = 0;
            } else {
            $idx = strpos($wpFullRel,$wpBaseRel);
            }
            if ($idx == 0) {
            $wpOut = substr($wpFullRel,strlen($wpBaseRel));	// remove URL base
            } else {
            $wpOut = NULL;	// base does not match beginning of full path; can't operate
            }
        }
        return $wpOut;
    }
    */

    // -- CALCULATIONS -- //
    // ++ PATHINFO ++ //

    /* 2024-08-11 Does not seem to be used by Invite.
    static public function GetPairHandlingArray($key,$val) {
        if ($val === TRUE) {
            $ar['key'] = $key;
            $ar['val'] = NULL;
            $ar['sep'] = FALSE;
        } elseif (($val === FALSE) || is_null($val)) {
            $ar['key'] = NULL;
            $ar['val'] = NULL;
            $ar['sep'] = FALSE;
        } else {
            if (is_string($val) or is_int($val)) {
                $ar['key'] = $key;
                $ar['val'] = $val;
                $ar['sep'] = TRUE;
            } else {
                // 2018-05-30 This seems to happen a lot, so let's have a message.
                $sType = fcArray::RenderElement($val);
                throw new exception("Ferreteria usage error: An ambiguous or nonscalar ($sType) was passed as the value for [$key].");
            }
        }
        return $ar;
    }
    */
    /* 2024-08-11 Does not seem to be used by Invite.
    static public function FromArray(array $arArgs=NULL,$sSep=KS_CHAR_URL_ASSIGN) {
        $fpArgs = NULL;
        foreach ($arArgs as $key => $val) {
            $arTerm = static::GetPairHandlingArray($key,$val);
            $sSepUse = $arTerm['sep']?$sSep:'';
            $sPart = $arTerm['key'].$sSepUse.$arTerm['val'];

            if (!is_null($sPart)) {
                $fpArgs .= $sPart.'/';
            }
        }
        return $fpArgs;
    }
    */
    /*----
      ACTION: Parses paths formatted using KS_CHAR_PATH_SEP and KS_CHAR_URL_ASSIGN
        1. Explode path into segments (folder names) using KS_CHAR_PATH_SEP
        2. Explode each segment into key/value pairs using KS_CHAR_URL_ASSIGN
    */
    /* 2024-08-11 Does not seem to be used by Invite.
    static public function ParsePath($sPath,$sPathSep=KS_CHAR_PATH_SEP,$sArgSep=KS_CHAR_URL_ASSIGN) {
        if (!is_string($sPath)) {
            throw new exception(__METHOD__.' was handed something other than a string for the path.');
        }
        $fp = fcString::GetBeforeFirst($sPath,'?');	// remove query, if any
        $fp = trim($fp,$sPathSep);			// remove beginning/ending path separators
        $arPath = explode($sPathSep,$fp);
        foreach ($arPath as $fn) {
            $arFrag = explode($sArgSep,$fn);	// argument separator
            $cnt = count($arFrag);
            $key = array_shift($arFrag);
            if ($cnt == 1) {
                // no value, just a key
                $val = TRUE;
            } elseif($cnt == 2) {
                // key and single value
                $val = array_shift($arFrag);
            } else {
                // key and multiple values (list)
                $val = $arFrag;
            }
            $arOut[$key] = $val;
        }
        return $arOut;
    }
    */

    // -- PATHINFO - //

  // // -- STATIC -- // //
}
