<?php namespace SpecialInvite\util;
/*
  HISTORY:
    2024-08-11 adapted from old Ferreteria code [formerly] used by this extension
*/

interface iAction {
    function SetMessage(string $s);
    function GetMessage() : string;
    function SetNumber(int $n);
    function GetNumber() : int;
    function WasTried() : bool;
    function SetOkay(bool $ok);
    function GetOkay() : bool;
    function GetStatusText() : string;
}

class cAction implements iAction {
    private $sMsg='';
    public function SetMessage(string $s) { $this->sMsg = $s; }
    public function GetMessage() : string { return $this->sMsg; }

    private $nErr=0;
    public function SetNumber(int $n) { $this->nErr = $n; }
    public function GetNumber() : int { return $this->nErr; }

      // was the action tried?
    private $isTried = FALSE;
    // if tried, did it succeed?
    private $isOkay;

    public function WasTried() : bool {
        return $this->isTried;
    }

    public function SetOkay(bool $ok) {
        $this->isTried = TRUE;
        $this->isOkay = $ok;
    }
    public function GetOkay() : bool {
        if ($this->isTried) {
            return $this->isOkay;
        } else {
            throw new exception('Ferreteria internal error: action status requested before action attempted.');
        }
    }

    public function GetStatusText() : string {
        if ($this->WasTried()) {
            if ($this->GetOkay()) {
            $out = 'Operation successful.';
            } else {
            $nErr = $this->GetNumber();
            $sErr = $this->GetMessage();
            $out = "Error #$nErr: $sErr.";
            }
        } else {
            $out = 'Operation has not been attempted yet!';
        }
        return $out;
    }

}
