<?php namespace SpecialInvite;
/*
  PURPOSE: Load this file from LocalSettings.php in order to be able to set the data path.
  THINKING: The MediaWiki JSON-based extension invocation apparently does not register classes for autoloading,
    so we have to explicitly load the options class before we can use it.
    I originally devised this format for w3tpl.
  HISTORY:
    2020-09-01 created to allow setting of data-file path
*/

use SpecialPage;
use Title;

class cOptions {
    static public function NameForExtension() : string { return 'SpecialInvite'; }  // used in error messages

    static private $fpData; // no default; do not include trailing '/'
    static public function SetFolder_forData(string $fp) { self::$fpData = $fp; }
    static public function GetFolder_forData() : string { return self::$fpData; }
    
    /*----
      PURPOSE: Allows config to determine how carefully to guard the password from onlookers
        * TRUE = never display it after it is submitted, keep it out of browser history (best for production use)
        * FALSE = ok to show it in URLs or on the page (good for debugging)
      TODO: support TRUE mode (only partly written; not tested)
    */
    static private $doHideInvite = FALSE;
    static public function GetDoHideInvite() : bool { return self::$doHideInvite; }
    static public function SetDoHideInvite(bool $b) { self::$doHideInvite = $b; }
    
    static public function GetNameString_forCreateAccount() : string { return 'CreateAccount'; }  // name of SpecialPage (n/i namespace)
    static public function GetTitle_forCreateAccount() : Title { return SpecialPage::getTitleFor(self::GetNameString_forCreateAccount()); } // there's probably a better way to get this
    static public function GetName_forInviteString() : string { return 'invite'; }

    static private $fnCodes = 'codes.json';
    static public function SetFile_forCodes(string $fn) { self::$fnCodes = $fn; }
    static public function GetFile_forCodes() : string { return self::$fnCodes; }
    static public function GetSpec_forCodes() : string { return self::GetFolder_forData().'/'.self::$fnCodes; }
}
