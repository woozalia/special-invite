## Folders
* **src/**
  * **data/** classoids for managing the invite-code data
  * **mw/** MediaWiki-interface classoids
  * **util/** general utility stuff that could be reused elsewhere

## Change History
* **2024-09-06** Tidying up & fixing accounts-uncreatable bug; ended up completely refactoring classes.
* **2024-09-07** AU bug fixed by re-enabling code I had commented out in 2021 as "apparent unnecessary", basically.

## extension.json

Official documentation: https://www.mediawiki.org/wiki/Manual:Extension.json/Schema

* **"manifest_version"**: this is tied to the MediaWiki version; currently must always be 2
* **"Hooks": {..}**: this is where we grab MediaWiki hooks
